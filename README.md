# Laboratorio de Datos

1er cuatrimestre 2024 \
Universidad de Buenos Aires

**Docentes:** Santiago Laplagne, Nazareno Faillace Mullen y Gonzalo Barrera Borla

## Prácticas

| Nro |                  Título                       | Enunciado                                                                                          | Solución                                                                                                      |
|-----|-----------------------------------------------|----------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------|
| 1   | Nociones básicas de Python y Numpy | [Enunciado](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/practicas/enunciados/practica1.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/practicas/soluciones/practica1.ipynb)
| 2   | DataFrames y Estadística descriptiva | [Enunciado](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/practicas/enunciados/practica2.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/practicas/soluciones/practica2.ipynb)
| 3   | Visualización de datos | [Enunciado](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/practicas/enunciados/practica3.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/practicas/soluciones/practica3.ipynb)
| 4   | Regresión lineal | [Enunciado](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/practicas/enunciados/practica4.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/practicas/soluciones/practica4.ipynb)
| 5   | Modelo lineal multivariado. Entrenamiento y testeo | [Enunciado](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/practicas/enunciados/practica5.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/practicas/soluciones/practica5.ipynb)
| 6   | Operaciones con DataFrames y preprocesamiento | [Enunciado](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/practicas/enunciados/practica6.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/practicas/soluciones/practica6.ipynb)
| 7   | Clustering | [Enunciado](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/practicas/enunciados/practica7.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/practicas/soluciones/practica7.ipynb)
| 8   | Componentes principales | [Enunciado](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/practicas/enunciados/practica8.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/practicas/soluciones/practica8.ipynb)
| 9   | Descenso por gradiente y redes neuronales | [Enunciado](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/practicas/enunciados/practica9.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/practicas/soluciones/practica9.ipynb)

## Teóricas
| Nro |                  Título                                                                                                       |
|-----|-------------------------------------------------------------------------------------------------------------------------------|
| 1   | [Objetivos de la materia](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/teoricas/clase1.pdf) |
| 2   | [Estadística descriptiva](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/teoricas/clase2.pdf) |
| 3   | [Introducción a Git](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/teoricas/clase3.pdf) |
|4 y 5| [Visualización de datos](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/teoricas/clase4.pdf) |
| 6   | [Regresión lineal simple](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/teoricas/clase6.pdf) |
| 7   | [Cuadrados mínimos](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/teoricas/clase7.pdf) |
| 8   | [Modelo lineal multivariado](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/teoricas/clase8.pdf) |
| 9   | [Modelo lineal multivariado](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/teoricas/clase9.pdf) |
| 10   | [Validación cruzada](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/teoricas/clase10.pdf) |
| 11   | [Regresión Ridge](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/teoricas/clase11.pdf) |
| 15   | [Clustering - KMeans](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/teoricas/clase15.pdf) |
| 16   | [Clustering - DBSCAN](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/teoricas/clase16.pdf) |
| 17   | [K-nearest neighbors](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/teoricas/clase17.pdf) |
| 18 y 19| [Principal Component Analysis](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/teoricas/clase18.pdf) |
| 20   | [Descenso por Gradiente Estocástico](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/teoricas/clase20.pdf) |
| 21   | [Redes Neuronales](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/teoricas/clase21.pdf) |




## Notebooks

| Nro |                  Título                                                                                                                                  |
|-----|----------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1   | [Introducción a Jupyter, Numpy, azar y archivos de datos](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/notebooks/clase1.ipynb) |
| 2 y 3   | [Pandas, Gapminder, Introducción a visualización y Estadística Descriptiva](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/notebooks/clase2.ipynb) |
| 4 y 5   | [Visualización de datos](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/notebooks/clase4.ipynb) |
| 6   | [Regresión lineal simple](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/notebooks/clase6.ipynb) |
| 7   | [Cuadrados mínimos](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/notebooks/clase7.ipynb) |
| 9   | [Entrenamiento y testeo](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/notebooks/clase9-ldd-entrenamiento.ipynb) |
| 10   | [Validación cruzada](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/notebooks/clase10-ldd-validacionCruzada.ipynb) |
| 11   | [Regresión Ridge](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/notebooks/clase11-ldd-regresionRidge.ipynb) |
| 12   | [Grid Search](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/notebooks/clase12-ldd-gridsearch.ipynb) |
| 13   | [Operaciones con DataFrames](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/notebooks/clase13-ldd-operaciones.ipynb) |
| 14   | [Preprocesamiento](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/notebooks/clase14-ldd-preprocesamiento.ipynb) |
| 15   | [Clustering - KMeans](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/notebooks/clase15-ldd-clusteringkmeans.ipynb) |
| 16   | [Clustering - DBSCAN](https://gitlab.com/faustomartinez/uba-laboratorio-de-datos/-/blob/main/notebooks/clase16-ldd-dbscan.ipynb) |
